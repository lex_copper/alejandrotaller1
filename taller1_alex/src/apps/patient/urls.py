from django.urls import path
from src.apps.patient.views import Index

urlpatterns = [
    path(r'index', Index.as_view(), name='index'),
]