from django.contrib import admin
from patient.models import Patient, Gender, CountryCode, PhoneType, EmailType, PatientPhone, PatientEmail,\
    Phone, Email
from django import forms


class PatientPhoneT(admin.TabularInline):
    model = PatientPhone
    extra = 1


class PatientEmailT(admin.TabularInline):
    model = PatientEmail
    extra = 1


class PhonesT(admin.TabularInline):
    model = Phone
    extra = 1


class PatientPhoneAdmin(admin.ModelAdmin):
    list_display = ('patient', 'priority', 'phones', 'phone_notes')


class PatientEmailAdmin(admin.ModelAdmin):
    list_display = ('patient', 'emails', 'email_notes')


class PhoneAdmin(admin.ModelAdmin):
    inlines = [PatientPhoneT, ]


def calculate_priority(instance, related_list, old, new):
    """Reordena la prioridad de los correos o telefonos excluyendo el que se
    esta modificando actualmente"""
    if old != new and new <= related_list.count():
        counter = 1
        ec_list = related_list.exclude(pk=instance.id)
        for ec in ec_list:
            if counter == new:
                counter += 1
            # if ec != instance:
            ec.priority = counter
            ec.save()
            counter += 1


class PatientAdmin(admin.ModelAdmin):
    list_display = ('identifier', 'full_name', 'gender', 'get_phones', 'get_emails')
    inlines = [PatientPhoneT, PatientEmailT]

    def save_related(self, request, form, inlines, change):
        phone_forms = inlines[0]
        email_forms = inlines[1]
        instance = form.instance
        super(PatientAdmin, self).save_related(request, form, inlines, change)
        for phone_form in phone_forms:
            if 'priority' in phone_form.changed_data:
                calculate_priority(phone_form.instance,
                                   PatientPhone.objects.filter(patient=instance),
                                   phone_form.initial.get('priority', 1),
                                   phone_form.cleaned_data['priority'])
            if 'DELETE' in phone_form.cleaned_data and phone_form.cleaned_data['DELETE']:
                calculate_priority(phone_form.instance,
                                   PatientPhone.objects.filter(patient=instance),
                                   phone_form.initial.get('priority', 1),
                                   0)
            if phone_form.cleaned_data and not phone_form.cleaned_data.get('id'):
                last_Phone = PatientPhone.objects.filter(patient=instance).count()
                calculate_priority(phone_form.instance,
                                   PatientPhone.objects.filter(
                                       patient=instance),
                                   phone_form.initial.get('priority', 1),
                                   last_Phone)
                phone_form.instance.priority = last_Phone if last_Phone else 1
                phone_form.instance.save()

        for email_form in email_forms:
            if 'priority' in email_form.changed_data:
                calculate_priority(email_form.instance,
                                   PatientEmail.objects.filter(patient=instance),
                                   email_form.initial.get('priority', 1),
                                   email_form.cleaned_data['priority'])
            if 'DELETE' in email_form.cleaned_data and email_form.cleaned_data['DELETE']:
                calculate_priority(email_form.instance,
                                   PatientEmail.objects.filter(patient=instance),
                                   email_form.initial.get('priority', 1),
                                   )
            if email_form.cleaned_data and not email_form.cleaned_data.get('id'):
                last_email = PatientEmail.objects.filter(patient=instance).count()
                calculate_priority(email_form.instance,
                                   PatientEmail.objects.filter(
                                       patient=instance),
                                   email_form.initial.get('priority', 1), last_email)
                email_form.instance.priority = last_email if last_email else 1
                email_form.instance.save()

    def get_phones(self, obj):
        """Funtion to get the phones related with the patient"""
        phone = None
        phone_list = []
        for p in obj.patientphone_set.all().order_by('priority'):
            phone_number = str(p.phones.phone_number)
            priority = str(p.priority)
            code = str(p.phones.country_code.code)
            phone_list.append(priority+': ' + ' + ' + '(' + code + ')' + phone_number)
            phone = ",  ".join(phone_list)
        return phone

    def get_emails(self, obj):
        """Funtion to get the emails related with the patient"""
        email = None
        email_list = []
        for p in obj.patientemail_set.all().order_by('priority'):
            email = str(p.emails.email)
            priority = str(p.priority)
            email_list.append(priority + ': ' + email)
            email = ",  ".join(email_list)
        return email


class CountryCodeAdmin(admin.ModelAdmin):
    list_display = ('code', 'country')


class GenderAdmin(admin.ModelAdmin):
    list_display = ('gender', )


class PhoneTypeAdmin(admin.ModelAdmin):
    list_display = ('description', )


class EmailTypeAdmin(admin.ModelAdmin):
    list_display = ('description', )


class EmailAdmin(admin.ModelAdmin):
    inlines = [PatientEmailT, ]


admin.site.register(Gender, GenderAdmin)
admin.site.register(CountryCode, CountryCodeAdmin)
admin.site.register(PhoneType, PhoneTypeAdmin)
admin.site.register(EmailType, EmailTypeAdmin)
admin.site.register(Phone, PhoneAdmin)
admin.site.register(Email, EmailAdmin)
admin.site.register(Patient, PatientAdmin)
admin.site.register(PatientPhone, PatientPhoneAdmin)
admin.site.register(PatientEmail, PatientEmailAdmin)




